import discord
from discord.ext import commands
import pickle

intents = discord.Intents.default()
intents.message_content = True

bot = commands.Bot(command_prefix='!system ', intents=intents)

def loadData(path):
	sys={}
	try:
		sys = pickle.load(open(path, "rb"))
	except (OSError, IOError) as e:
		sys = {}
		pickle.dump(sys, open(path, "wb"))
	return sys

def saveData(sys, path):
	with open(path,"wb") as f:
		pickle.dump(sys, f)


# system files

paoPath="pao.pickle"
centPath="cent.pickle"
millPath="mill.pickle"
piqPath="piq.pickle"
piFile="pi.txt"

with open (piFile, "r") as myfile:
    dataPi=myfile.readlines()[0]


paoS = loadData(paoPath)
print(paoS)
centiumS = loadData(centPath)
print(centiumS)
milleniumS = loadData(millPath)
piRec = loadData(piqPath)

#record files (1 per discipline)
mlPath = "mlRecords.pickle"
mlRecords = loadData(mlPath)
mlDisciplines=["numbers", "words", "images", "intnames", "names", "cards"]


def checkPi(val):
	score = 0
	for i in range(len(val)):
		if(dataPi[i] != val[i]):
			break
		score += 1
	return score

@bot.command()
async def piquizz(ctx, acc, val=None):
	"""
	piquizz game: !system help piquizz for more details
		play <decimals> : provide as many decimals as you can remember to enter the leaderboard!
		ranking: view the top 10 scores
		ranking <username>: rank and PB of player with username "username"
	"""
	if(acc == "play" and val != None):
		score = checkPi(val)
		un = str(ctx.author.name)

		if (un in piRec):
			if(score > piRec[un]):
				oldPB = piRec[un]
				piRec[un] = score
				await ctx.send("New PB! "+str(oldPB)+" -> "+str(score))
			else:
				await ctx.send("Great try, final score: "+str(score)+". PB: "+str(piRec[un]))
		else:
			piRec[un] = score
			await ctx.send("New contender! final score: "+str(score))
			
		saveData(piRec, piqPath)
	elif(acc == "ranking"):
		if(val == None):
			stret = "Top 10 players:\n"
			i = 0
			for k, v in sorted(piRec.items(), key=lambda item: item[1]):
				if(i>10):
					break
				stret += "\t"+k+": "+str(v)+"\n"
			await ctx.send(stret)
		else:
			if(val in piRec.keys()):
				await ctx.send(val+" PB: "+str(piRec[val]))
			else:
				await ctx.send("User "+val+" not registered ;)")
			

@bot.command()
async def records(ctx, acc, cat, disci, username=None, score=None, time=None):
	if(not disci in mlDisciplines):
		await ctx.send("Discipline must be one of "+", ".join(mlDisciplines))
	elif(cat=="ML" and acc == "set"):
		if(username==None or score==None or time==None):
			await ctx.send("You must provide username, score, and time to register your personnal best")
		else:
			if(not disci in mlRecords):
				mlRecords[disci] = {}
#			
#			if(username in mlRecords[disci] and (float(score)<mlRecords[disci][username][0] or (float(score)==mlRecords[disci][username][0] and float(time) > mlRecords[disci][username][1]))):
#				await ctx.send("You already registered a better score :)")
#			else:
			try:
				a = float(score)
				b = float(time)
				mlRecords[disci][username]=[a, b]
				await ctx.send("Thanks for registering your PB, soon available at https://clementcs.gitlab.io/memorybot/records/ ;)")
			except ValueError:
				await ctx.send("register correct values please :)")
		saveData(mlRecords, mlPath)

	elif(cat=="IAM" and acc=="set"):
		await ctx.send("TODO, wait a little bit :)")
	elif(acc=="get"):
		if(cat=="ML"):
			if(disci in mlRecords):
				print(mlRecords[disci])
				await ctx.send("You can view all registers best scores for ML at: https://clementcs.gitlab.io/memorybot/records/")
				s = "**player score time**\n"
				for e in sorted(mlRecords[disci], key=lambda x : (-(mlRecords[disci][x][0]), (mlRecords[disci][x][1]))):
					s += "**"+e+":** "+str(mlRecords[disci][e][0])+" "+str(mlRecords[disci][e][1])+"\n"
				await ctx.send(s)
		else:
			await ctx.send("TODO, come back later :)")

@bot.command()
async def backup(ctx):
	"""
	Link to the code and backups of data files
	"""
	await ctx.send("Find backups of the community's systems and the code of the bot at: https://gitlab.com/clementcs/memorybot")

@bot.command()
async def list(ctx):
	"""
	Access whole systems
	"""
	await ctx.send("See you on https://clementcs.gitlab.io/memorybot/ (not updated instantaneously, it requires a manual update on my side)")

@bot.command()
async def millenium(ctx, acc, numb=None, *, val=None):
	"""
	millenium subcommand: !system help millenium for more details
		set <number> <value>: adds the value associated to number number to the collection
		get <number>: fetch all values corresponding to a number
		list: dump the content of the collection
	"""
	if(numb==None or int(numb) < 0 or int(numb) > 1000):
		await ctx.send("Specify a number between 0 and 100 :)")
	elif(acc == "delete"):
		if(int(numb) in milleniumS and val in milleniumS[int(numb)]):
			milleniumS[int(numb)].remove(val)
			await ctx.send("Removed element, thanks!")
		else:
			await ctx.send("Nothing to remove")
	elif(acc == "get"):
		if(int(numb) in milleniumS):
			await ctx.send("**"+numb+": ** "+", ".join(milleniumS[int(numb)]))
		else:
			await ctx.send("Nothing at the moment, don't hesitate to add yours :)")
	elif(acc == "set"):
		if(not numb.isnumeric()):
			await(ctx.send("can only register new values for a number"))
		elif(val==None):
			await ctx.send("You must specify a value. Ex: set 014 sauterelle")
		else:
			if(not int(numb) in milleniumS):
				milleniumS[int(numb)] = [val]
			else:
				if(not val in milleniumS[int(numb)]):
					milleniumS[int(numb)].append(val)
			saveData(milleniumS, millPath)
			await ctx.send("Thanks for adding values to the millenium collection :) Soon at: https://clementcs.gitlab.io/memorybot/systems/millenium/")


@bot.command()
async def centium(ctx, acc, numb=None, *, val=None):
	"""
	centium subcommand: !system help centium for more details
		set <number> <value>: adds the value associated to number number to the collection
		get <number>: fetch all values corresponding to a number
		list: dump the content of the collection
	"""
	if(numb==None or int(numb) < 0 or int(numb) > 100):
		await ctx.send("Specify a number between 0 and 100 :)")
	elif(acc == "delete"):
		if(int(numb) in centiumS and val in centiumS[int(numb)]):
			centiumS[int(numb)].remove(val)
			await ctx.send("Removed element, thanks!")
		else:
			await ctx.send("Nothing to remove")
	elif(acc == "get"):
		if(int(numb) in centiumS):
			await ctx.send("**"+numb+": ** "+",".join(centiumS[int(numb)]))
		else:
			await ctx.send("Nothing at the moment, don't hesitate to add yours :)")
	elif(acc == "set"):
		if(val==None):
			await ctx.send("You must specify a value. Ex: set 01 sauterelle")
		else:
			if(not int(numb) in centiumS):
				centiumS[int(numb)] = [val]
			else:
				if(not val in centiumS[int(numb)]):
					centiumS[int(numb)].append(val)
			saveData(centiumS, centPath)
			await ctx.send("Thanks for adding values to the centium collection :) Soon at: https://clementcs.gitlab.io/memorybot/systems/centium/")

@bot.command()
async def pao(ctx, acc, numb=None, elem=None, *, val=None):
	"""
	centium subcommand: !system help pao for more details
		set <number> <elem> <value>: adds the value associated to number number and element elem (either P, A, O, ...) to the collection
		get <number> [elem]: fetch all values corresponding to a number (if P not specified, returns all elems)
		list: dump the content of the collection
	"""
	print("{} {}".format(acc,elem))
	if(numb != None and not numb.isnumeric()):
		await ctx.send("only acts on numbers")
	elif(numb != None):
		numb = int(numb)
	
	if(acc == "delete"):
		if(numb in paoS and val in paoS[numb][elem]):
			if(val in paoS[numb][elem]):
				paoS[numb][elem].remove(val)
				await ctx.send("Removed element, thanks!")
		else:
			await ctx.send("Nothing to remove")
	elif(acc == "get" and numb!=None):
		if(elem == None and numb in paoS):
			s = "**"+str(numb)+": **"
			for e in paoS[numb]:
				s+="\n\t**"+str(e)+": **"+', '.join(sorted(paoS[numb][e]))
			await ctx.send(s)
		elif(numb in paoS and elem in paoS[numb]):
			await ctx.send("{}{}: {}".format(elem,numb,", ".join(paoS[numb][elem])))
		else:
			await ctx.send("{}{}: nothing at the moment (set it yourself :) )".format(elem,numb))
	elif(acc == "set" and numb != None):
		if(not (elem=="P" or elem=="A" or elem=="O")):
			await ctx.send("pao consists in P, A and O, not "+elem)
		elif(elem==None or val==None):
			await ctx.send("To set a value you must specify an element (P, A, O, ...)")
		else:
			if(not numb in paoS):
				paoS[numb]={}
			if(not elem in paoS[numb]):
				paoS[numb][elem] = [val]
			else:
				if(not val in paoS[numb][elem]):
					paoS[numb][elem].append(val)
			saveData(paoS, paoPath)
			await ctx.send("Thanks for your contribution :) Soon at: https://clementcs.gitlab.io/memorybot/systems/pao/")

@bot.event
async def on_ready():
	print('starting')

@bot.event
async def on_disconnect():
	print("exit")

bot.run('your token :)')
