---
title: "ML ranking names"
date: 2020-11-15T14:38:40+01:00
draft: false
---

user | score | time
:------|:-----|:------------
**SylvainArvidieu** |30.0 | 49.36
**GuillaumePJ** |30.0 | 60.0
**clem** |28.0 | 59.76
**ArnaudFegman** |22.0 | 59.48
**Anthony** |21.0 | 60.0
