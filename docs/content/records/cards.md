---
title: "ML ranking cards"
date: 2020-11-15T14:38:40+01:00
draft: false
---

user | score | time
:------|:-----|:------------
**SylvainArvidieu** |52.0 | 19.68
**clem** |52.0 | 33.45
**GuillaumePJ** |52.0 | 49.29
**Anthony** |45.0 | 60.0
**ArnaudFegman** |37.0 | 58.42
