---
title: "ML ranking images"
date: 2020-11-15T14:38:40+01:00
draft: false
---

user | score | time
:------|:-----|:------------
**clem** |30.0 | 11.57
**SylvainArvidieu** |30.0 | 13.15
**GuillaumePJ** |30.0 | 16.94
**Anthony** |30.0 | 24.75
**ArnaudFegman** |30.0 | 26.03
