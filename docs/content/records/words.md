---
title: "ML ranking words"
date: 2020-11-15T14:38:40+01:00
draft: false
---

user | score | time
:------|:-----|:------------
**SylvainArvidieu** |50.0 | 55.96
**clem** |47.0 | 60.0
**GuillaumePJ** |43.0 | 60.0
**Anthony** |32.0 | 60.0
**ArnaudFegman** |28.0 | 59.68
