---
title: "ML ranking numbers"
date: 2020-11-15T14:38:40+01:00
draft: false
---

user | score | time
:------|:-----|:------------
**SylvainArvidieu** |80.0 | 20.62
**clem** |80.0 | 23.22
**GuillaumePJ** |80.0 | 29.79
**Anthony** |80.0 | 43.73
**ArnaudFegman** |73.0 | 58.98
