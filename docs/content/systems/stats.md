---
title: "stats"
date: 2020-11-15T14:38:40+01:00
draft: false
---

## Stats for system Millenium
Total number of values : 1818

Average number of values per number overall : 1.818

Number with maximum number of words : 854 (19 values)

Values with no association: 

Density Plot:

![Density plot](/systems/statPlot_Millenium.svg "Millenium density")By range : 

range | total number of values | avg number of values
:------|:----------------------|---------------------
0 - 100 | 172 | 1.72
100 - 200 | 231 | 2.31
200 - 300 | 144 | 1.44
300 - 400 | 174 | 1.74
400 - 500 | 160 | 1.6
500 - 600 | 162 | 1.62
600 - 700 | 131 | 1.31
700 - 800 | 268 | 2.68
800 - 900 | 187 | 1.87
900 - 1000 | 189 | 1.89
## Stats for system PAO
Total number of values : 358

type | total number of values 
:------|:----------------------
P | 113
A | 104
O | 141
Keys with no value: 22:A, 23:O, 25:O, 29:A, 56:P, 65:P, 66:P, 66:A

Density Plot for P values:

![Density plot](/systems/statPlot_PAO_P.svg "PAO density")

Density Plot for A values:

![Density plot](/systems/statPlot_PAO_A.svg "PAO density")

Density Plot for O values:

![Density plot](/systems/statPlot_PAO_O.svg "PAO density")

