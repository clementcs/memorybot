---
title: "Centium"
date: 2020-11-15T14:38:40+01:00
draft: false
---

nombre | valeurs associées
:------|:-----------------
0 | Saucisse
1 | Sauterelle, Stmg, Zidane, Citron
2 | Songoku, Sun, Cène (Da Vinci Dernier Repas Du Christ), Sonic
3 | Sumo, Samael, Sumo
4 | Sardine, Soirée
5 | Salopette, Solary
6 | Souche, Sch
7 | Cigarette, Skt1, Gant
8 | Savon, Saphir
9 | Sapeur, Espion, Superman
10 | Tasse, Tasse
11 | Tetine, Twitter
12 | Dinosaure
13 | Tam-Tam
14 | Taré
15 | Téléphone
16 | Douche, Johnny
17 | Tektonik, Duc
18 | Touffe, Tofu
19 | Tymbale
20 | Anis
21 | Nutella, Natte
22 | None, Ananas, Nonne, Nana
23 | Nem
24 | Narval
25 | Nihiliste
26 | Niche, Nageur
27 | Nicolas
28 | Navigateur, Hénaff
29 | Napolitain, Noble
30 | Masse, Mousse
31 | Méduse, Mouton
32 | Moine, Mine
33 | Mamie
34 | Marriage, Marguerite, Miroir
35 | Moule, Malabar
36 | Mouche
37 | Macdo, Magritte
38 | Mouffles
39 | Mobylette, Meuble, Map
40 | Rose, Rousse
41 | Rototo, Rateau
42 | Urine, Araignée
43 | Rame
44 | Rire, Aurore (Boréale)
45 | Rola-Bola
46 | Rocher, Ruche
47 | Haricot, Raquette, Raclette
48 | Ravioli, Ronfler
49 | Rappeur
50 | Laisse, Lance, Lasso
51 | Loutre, Lutin
52 | Lune, Laine
53 | Lama, Lime, Lame, Limo
54 | Lardon
55 | Lila
56 | Louche
57 | Lac, Légume
58 | Leffe, Lave
59 | Loupe
60 | Chaise
61 | Chaton
62 | Chaine
63 | Chameau, Chamois, Gemme, Cheminée, Gymnaste, Gym
64 | Shrek
65 | Gel
66 | Chouchou
67 | Chèque, Chicot
68 | Chou-Fleur
69 | Chapeau, Japonais
70 | Coussin
71 | Gâteau
72 | Canne, Canard, Canari
73 | Caméra
74 | Carrelage, Car
75 | Claque, Colique
76 | Cachalot
77 | Cactus
78 | Coffre, Café, Gauffre
79 | Capitaine, Cable
80 | Fesse
81 | Foot
82 | Fenwick
83 | Fumer, Vomi
84 | Four, Frite
85 | Velouté, Foule
86 | Vache
87 | Facteur, Vaccin, Fax
88 | Favoris, Fifa
89 | Fabrique
90 | Bouse, Buzz, Puce, Pez
91 | Pantalon
92 | Benne
93 | Pomme
94 | Parapente, Prêtre
95 | Pile, Pole-Emploi
96 | Pêche, Biche
97 | Bouc, Pique, Pacman
98 | Pif, Boeuf
99 | Babar, Pape
