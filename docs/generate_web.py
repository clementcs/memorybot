import pickle
import matplotlib.pyplot as plt

def loadSystem(path):
	sys={}
	try:
		sys = pickle.load(open(path, "rb"))
	except (OSError, IOError) as e:
		sys = {}
		pickle.dump(sys, open(path, "wb"))
	return sys

def saveSystem(sys, path):
	with open(path,"wb") as f:
		pickle.dump(sys, f)

def printMillenium(name, sys,path):
	with open(path, "w") as file:
		file.write("---\ntitle: \""+name+"\"\ndate: 2020-11-15T14:38:40+01:00\ndraft: false\n---\n\n")
		file.write("nombre | valeurs associées\n")
		file.write(":------|:-----------------\n")
		for k in sorted(sys):
			file.write(str(k)+" | "+str(", ".join(sys[k]).title()+"\n"))

def statsSys(sysMill, sysPAO, path):
	with open(path+"/stats.md", "w") as file:
		millKeys = [i for i in range(0, 1000)]
		millNbVals = []

		for i in millKeys:
			if (not i in sysMill):
				millNbVals.append(0)
			else:
				#print(sys[i])
				millNbVals.append(len(sysMill[i]))
		file.write("---\ntitle: \"stats\"\ndate: 2020-11-15T14:38:40+01:00\ndraft: false\n---\n\n")

		file.write("## Stats for system Millenium\n")
		file.write("Total number of values : "+str(sum(millNbVals))+"\n\n")
		file.write("Average number of values per number overall : "+str(sum(millNbVals) / len(millKeys))+"\n\n")

		file.write("Number with maximum number of words : "+str(millNbVals.index(max(millNbVals)))+" ({} values)\n\n".format(max(millNbVals)))
		noAss = []
		for i in millKeys:
			if (millNbVals[i]==0):
				noAss.append(str(i))

		file.write("Values with no association: "+str(", ".join(noAss))+"\n\n")

		generatePlot("{}/statPlot_{}.svg".format(path, "Millenium"), millKeys, millNbVals)
		file.write("Density Plot:\n\n")
		file.write("![Density plot](/systems/statPlot_{}.svg \"Millenium density\")".format("Millenium"))

		file.write("By range : \n\n")
		file.write("range | total number of values | avg number of values\n")
		file.write(":------|:----------------------|---------------------\n")
		for i in range(0, 1000, 100):
			sl = millNbVals[i:i+100]
			file.write(str(i)+" - "+str(i+100)+" | "+str(sum(sl))+" | "+str(sum(sl)/len(sl))+"\n")

		# same but for PAO
		file.write("## Stats for system PAO\n")
		totnb = {"P":0,"A":0,"O":0}
		paoKeys = [i for i in range(0, 100)]
		paoNBVals = []
		for i in paoKeys:
			paoNBVals.append({"P":0,"A":0,"O":0})
			for c in ["P","A","O"]:
				if ((not i in sysPAO) or (not c in sysPAO[i])):
					paoNBVals[i][c] = 0
				else:
					#print(sys[i])
					paoNBVals[i][c] = len(sysPAO[i][c])
					totnb[c] += len(sysPAO[i][c])
		file.write("Total number of values : "+str(totnb["P"]+totnb["A"]+totnb["O"])+"\n\n")

		file.write("type | total number of values \n")
		file.write(":------|:----------------------\n")
		file.write("P | "+str(totnb["P"])+"\n")
		file.write("A | "+str(totnb["A"])+"\n")
		file.write("O | "+str(totnb["O"])+"\n")

		noVals = ""
		for i in paoKeys:
			for c in ["P","A","O"]:
				if(paoNBVals[i][c] == 0):
					noVals += str(i)+":"+c+", "
		file.write("Keys with no value: "+noVals[:-2]+"\n\n")

		for c in ["P","A","O"]:
			density = [p[c] for p in paoNBVals]
			generatePlot("{}/statPlot_PAO_{}.svg".format(path, c), [i for i in range(100)], density)
			file.write("Density Plot for {} values:\n\n".format(c))
			file.write("![Density plot](/systems/statPlot_PAO_{}.svg \"PAO density\")\n\n".format(c))

												
def generatePlot(path, x, y):
	plt.figure(figsize=[20, 5])
	# plotting a bar chart
	plt.bar(x, y, width = 1)
	# x-axis label
	plt.xlabel('number')
	# frequency label
	plt.ylabel('No. of words')
	# plot title
	plt.title('Words density')
	# function to show the plot
	plt.savefig(path)

def printPAO(name, sys, path):
	with open(path, "w") as file:
		file.write("---\ntitle: \""+name+"\"\ndate: 2020-11-15T14:38:40+01:00\ndraft: false\n---\n\n")
		file.write("nombre | item| valeurs associées\n")
		file.write(":------|:-----|:------------\n")
		for k in sorted(sys):
			for c in ["P","A","O"]:
				if(c in sys[k]):
					file.write(str(k)+" | "+str(c)+" | "+str(",".join(sys[k][c]).title()+"\n"))

def printMLRecords(name, data, path):
	with open(path, "w") as file:
		file.write("---\ntitle: \"ML ranking "+name+"\"\ndate: 2020-11-15T14:38:40+01:00\ndraft: false\n---\n\n")
		file.write("user | score | time\n")
		file.write(":------|:-----|:------------\n")
		for e in sorted(data[name], key=lambda x : (-(data[name][x][0]), (data[name][x][1]))):
			file.write("**"+e+"** |"+str(data[name][e][0])+" | "+str(data[name][e][1])+"\n")
		print(data[name])

millPath="../backup/mill.pickle"
milleniumS = loadSystem(millPath)
docFile="content/systems/millenium.md"
printMillenium("Millenium",milleniumS, docFile)

centP="../backup/cent.pickle"
centS = loadSystem(centP)
docFile="content/systems/centium.md"
printMillenium("Centium", centS, docFile)

paoP="../backup/pao.pickle"
paoS = loadSystem(paoP)
docFile="content/systems/pao.md"
printPAO("PAO", paoS, docFile)

statsSys(milleniumS, paoS, "content/systems/")

name="images"
dataF="../backup/mlRecords.pickle"
data = loadSystem(dataF)
docFile="content/records/images.md"
printMLRecords(name,data,docFile)
name="words"
docFile="content/records/words.md"
printMLRecords(name,data,docFile)
name="cards"
docFile="content/records/cards.md"
printMLRecords(name,data,docFile)
name="names"
docFile="content/records/names.md"
printMLRecords(name,data,docFile)
name="intnames"
docFile="content/records/intnames.md"
printMLRecords(name,data,docFile)
name="numbers"
docFile="content/records/numbers.md"
printMLRecords(name,data,docFile)
